# ennerd/gitutils

Small git tools for lazy people.

## g-patch-push

This tool runs `git commit` if needed, then `git tag x.y.z` where the version was bumped by 0.0.1, finally it runs `git push origin master x.y.z`.

It is only supposed to be helpful when you make a tiny change to some library, and you should use git properly the rest of the time. :)

### Usage

```
# g-patch-push
```

Then follow the instructions...


## g-each

This tool runs git in each direct subdir. It accepts all the same arguments as git.

### Usage

```
# g-each status --porcelain
```

Will list all subdirectories and clearly show if they have files that need to be committed.


## Installation

```
# composer global require ennerd/gitutils
```


