<?php
declare(strict_types=1);

namespace Ennerd\GitUtils;

class lib
{
    public static function git_fetch()
    {
        echo '--- '.trim('git fetch')."\n";
        passthru('git fetch');
    }

    public static function git_is_ahead(): bool
    {
        $res = shell_exec('git status');

        return false !== stripos($res, 'ahead');
    }

    public static function git_is_behind(): bool
    {
        $res = shell_exec('git status');

        return false !== stripos($res, 'behind');
    }

    public static function git_has_origin(): bool
    {
        $res = shell_exec('git config --get remote.origin.url');
        if (null === $res) {
            return false;
        }

        return '' !== trim($res);
    }

    /**
     * true if everything have been added to the commit.
     *
     * @return void
     */
    public static function git_is_dirty(): bool
    {
        $dirty = false;
        $statuses = static::git_status();

        foreach ($statuses as $status) {
            switch ($status['marks'][1]) {
                case ' ':
                    break;
                case 'M':
                    return true;
                case 'D':
                    return true;
                case '?':
                    break;
                default:
                    return $status['filename'].' has an unknown status '.$status['marks'][1];
            }
        }

        return false;
    }

    public static function git_needs_commit()
    {
        $statuses = static::git_status();

        foreach ($statuses as $status) {
            switch ($status['marks'][1]) {
                case '?':
                    break;
                default:
                    return true;
            }
        }

        return false;
    }

    public static function git_commit(string $extra = '')
    {
        echo '--- '.trim("git commit $extra")."\n";
        passthru(trim('git commit '.$extra), $retv);

        return $retv;
    }

    public static function git_branch()
    {
        return trim(shell_exec('git branch'), "* \n\r\t");
    }

    public static function git_status()
    {
        $status = shell_exec('git status --porcelain=v1');
        if (null === $status) {
            return [];
        }
        $statusLines = explode("\n", trim($status, "\n\r"));
        $result = [];
        foreach ($statusLines as $line) {
            $tags = substr($line, 0, 2);
            $filename = trim(substr($line, 3));
            $result[] = ['marks' => $tags, 'filename' => $filename];
        }

        return $result;
    }

    public static function git_tag(string $extra = '')
    {
        if ('' !== $extra) {
            echo '--- '.trim("git tag $extra")."\n";
            passthru(trim('git tag '.$extra), $retv);
        } else {
            $res = shell_exec('git tag');
            if (null === $res) {
                return [];
            }

            return explode("\n", trim($res));
        }

        return $retv;
    }

    public static function git_push(string $extra = '')
    {
        echo "--- git push $extra\n";
        passthru(trim('git push '.$extra), $retv);

        return $retv;
    }

    public static function git_latest_semver()
    {
        $latest = '0.0.0';
        foreach (static::git_tag() as $version) {
            $parts = \count(explode('.', $version));
            if ('' !== trim($version, '0123456789.') || $parts < 1 || $parts > 3) {
                echo "Can't work with tags such as '$version'. Only works with numeric versions like '0.0.31' or '1.2.4'.";
                exit(2);
            }
            if (version_compare($latest, $version) < 0) {
                $latest = $version;
            }
        }

        return $latest;
    }

    public static function log(string $message)
    {
        echo ' *  '.trim($message)."\n";
    }
}
